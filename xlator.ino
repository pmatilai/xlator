static int cable = 0;

static int led = 13;
static int pedal1 = 14;
static int pedal2 = 15;

static int pyramidi = 16; /* PyraMIDI channel */

static void sendMMC(byte what, int cable)
{
  byte data[] = { 0x7f, 0x7f, 0x06, what };
  usbMIDI.sendSysEx(4, data, false, cable);
}

static void sendPedalOn(int pedal)
{
  digitalWrite(pedal, HIGH);
}

static void sendPedalOff(int pedal)
{
  digitalWrite(pedal, LOW);
}

static void sendCC(int cc, int channel, int cable)
{
  usbMIDI.sendControlChange(cc, 1, pyramidi, cable);
}

void myNoteOff(byte channel, byte note, byte velocity)
{
  switch (note) {
    case 94:  /* Mackie play */
      sendPedalOff(pedal1);
      break;
    case 93:  /* Mackie stop */
      sendPedalOff(pedal1);
      break;
    case 95:  /* Mackdie rec */
      sendPedalOff(pedal2);
      break;
  }
}

void myNoteOn(byte channel, byte note, byte velocity)
{
  digitalWrite(led, HIGH);
  switch (note) {
    case 94:  /* Mackie play */
      // sendPedalOn(pedal1);
      usbMIDI.sendRealTime(usbMIDI.Start);
      sendMMC(0x2, cable);
      break;
    case 93:  /* Mackie stop */
      // sendPedalOn(pedal1);
      usbMIDI.sendRealTime(usbMIDI.Stop);
      sendMMC(0x1, cable);
      break;
    case 92:  /* Mackie fast forward */
      sendCC(68, channel, cable);
      sendMMC(0x4, cable);
      break;
    case 91:  /* Mackie rewind */
      sendCC(67, channel, cable);
      sendMMC(0x5, cable);
      break;
    case 95:  /* Mackdie rec */
      sendCC(66, channel, cable);
      sendMMC(0x6, cable);
      break;
    default:
      /* debug only */
      break;
  }
  delay(5);
  digitalWrite(led, LOW);
}

void doXV3080Sysex(byte *data, unsigned int length)
{
  /* translate temporary performance patch edits to per-channel msb/lsb/pc */ 
  if (length > 12 && data[5] == 0x12 && data[6] == 0x10 && data[7] == 0x00) {
    byte part = data[8] - 31;
    byte target = data[9];

    if (target == 0x04) {
      byte msb = data[10];
      byte lsb = data[11];
      byte pc = data[12];
      usbMIDI.sendControlChange(0, msb, part);
      usbMIDI.sendControlChange(32, lsb, part);
      usbMIDI.sendProgramChange(pc, part);
    }
  }
}

void doRolandSysex(byte *data, unsigned int length)
{
  if (length > 4 && data[3] == 0x00 && data[4] == 0x10)
    doXV3080Sysex(data, length);
}

void mySystemExclusive(byte *data, unsigned int length)
{
  switch (data[1]) {
  case 0x41:
    digitalWrite(led, HIGH);
    doRolandSysex(data, length);
    delay(5);
    digitalWrite(led, LOW);
    break;
  default:
    break;
  }
}

void setup() 
{
  pinMode(led, OUTPUT);
  pinMode(pedal1, OUTPUT);
  pinMode(pedal2, OUTPUT);

  digitalWrite(led, HIGH);
  delay(10);
  usbMIDI.setHandleNoteOn(myNoteOn);
  usbMIDI.setHandleNoteOff(myNoteOff);
  usbMIDI.setHandleSystemExclusive(mySystemExclusive);

  digitalWrite(led, LOW);
}

void loop()
{
  usbMIDI.read();
}

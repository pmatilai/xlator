
A Teensy LC project for various ad-hoc MIDI-related translation needs in my
home studio. Unlikely to be useful to others as-is, except maybe for ideas
and examples.

Currently implemented translations:
- Mackie Control Transport to MIDI MMC
- Mackie Control Transport to PyraMIDI + pedal actions
- Roland XV-3080 Performance patch change SysEx messages to MSB/LSB/PC for
  the Pyramid, to allow patch selection via the synth patch finder.
